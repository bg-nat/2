﻿Param(
    [string]$username,
    [string]$password
)

$users = Get-ADUser -Filter {mobile -notlike "нет номена в 1с" -and mail -like "*"} -Properties mobile ,mail -SearchBase "OU=UsersNAT,DC=bg-nat,DC=com" | Select-Object -Property UserPrincipalName, mobile ,mail

$secureStringPwd = $password | ConvertTo-SecureString -AsPlainText -Force
$creds = New-Object System.Management.Automation.PSCredential -ArgumentList $username, $secureStringPwd

Import-Module MSOnline

Connect-MsolService –Credential $creds
$auth = New-Object -TypeName Microsoft.Online.Administration.StrongAuthenticationRequirement
$auth.RelyingParty = "*"
$auth.State = "Enabled"


Foreach ($user in $users)
{
    try{
        $user.mail
        $msolUser = Get-MsolUser -UserPrincipalName $user.mail
        $stateMFA =  ($msolUser.StrongAuthenticationRequirements).State
        if($stateMFA -ne 'Enforced'){
            Write-Host "Enabled MFA $user.mail"
            Set-MsolUser -UserPrincipalName $user.mail -StrongAuthenticationRequirements $auth
        }
    }
    catch{

    }
}
